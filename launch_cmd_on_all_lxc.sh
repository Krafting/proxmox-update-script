#!/bin/bash
# Launch the same command to all container

# list of container ids we need to iterate through
containers=$(pct list | tail -n +2 | cut -f1 -d' ')

function cmd_container() {
  container=$1
  echo "[Info] Launching command inside container $container"
  # to chain commands within one exec we will need to wrap them in bash
  pct exec $container -- bash -c "cat /etc/*-release | grep NAME"
}

for container in $containers
do
  status=`pct status $container`
  if [ "$status" == "status: stopped" ]; then
    echo [Info] Starting $container
    pct start $container
    echo [Info] Sleeping 5 seconds
    sleep 5
    cmd_container $container
    sleep 5
    echo [Info] Shutting down $container
    pct shutdown $container &
  elif [ "$status" == "status: running" ]; then
    cmd_container $container
  fi
done; wait
