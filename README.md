# Krafting's Proxmox Update Script

This is a script to update most of my LXC and proxmox at once.

This remove the need to SSH in each and every CT to update them.

*Warning: commands will be run as root, be careful before executing the script*

## Get the script

You can find the script in this repository [here](./update_all_lxc.sh)

You can get the script onto your server by using wget and downloading the script like so:

```bash
wget https://gitlab.com/Krafting/proxmox-update-script/-/raw/main/update_all_lxc.sh
```

## Executing the script

After downloading the script you can use the first command to make it executable and the second one to launch the script:

```
chmod +x update_all_lxc.sh
./update_all_lxc.sh
```

## Variables

Variables at the beginning of the script that you can change to make the script fit your setup better:

*Note: Every container is supposedly a Debian container if not in a custom CT type*

```md
# Custom CTs types:
ALPINE_CT : Put the ID of your CTs running Alpine.
PIHOLE_CT : CTs that has PiHole installed on a Debian CT (run an extra command to update pihole).
ARCH_CT : Put the ID of your CTs running Arch Linux.

# Bonus options:
SKIP_CT : CTs to NOT update.
PROXMOX_UPDATE : If set to yes, will update the Proxmox host.
WAKEUP_CT : If set to yes and a CT is turned-off, this will wake up the CT before updating it, then shutting it down again.
```

## Contribute

Feel free to contribute to add more type of LXC to update with this script, PR are opens!
