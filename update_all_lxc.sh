#!/bin/bash
# update all containers
# list of container ids we need to iterate through
containers=$(pct list | tail -n +2 | cut -f1 -d' ')

# Special containers
ALPINE_CT=()
PIHOLE_CT=()
ARCH_CT=()

# Container to NOT update
SKIP_CT=()

# Do we also update Proxmox ? (yes/no)
PROXMOX_UPDATE="yes"

# Should we wakeup and update down CTs ? (yes/no)
WAKEUP_CT="yes"

# List of fun terminal colors
RED='\033[0;31m'
LIGHT_RED='\033[1;31m'
GREEN='\033[0;32m'
LIGHT_GREEN='\033[1;32m'
ORANGE='\033[0;33m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
LIGHT_BLUE='\033[1;34m'
PURPLE='\033[0;35m'
LIGHT_PURPLE='\033[1;35m'
CYAN='\033[0;36m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'

# Function to check arrays
function array_contains () { 
    local array="$1[@]"
    local seeking=$2
    local in=1
    for element in "${!array}"; do
        if [[ $element == "$seeking" ]]; then
            in=0
            break
        fi
    done
    return $in
}

# Update Debian-based containers
function update_container_deb() {
        container=$1
        pct exec $container -- bash -c "apt update && apt full-upgrade -y && apt clean && apt autoremove -y"
}
# Update Alpine-base containers
function update_container_alpine() {
        container=$1
        pct exec $container -- sh -c "apk update && apk upgrade"
}
# Update Debian-based container with pihole on it.
function update_container_pihole() {
        container=$1
        pct exec $container -- bash -c "apt update && apt full-upgrade -y && apt clean && apt autoremove -y && /usr/local/bin/pihole -up"
}

# Function to update containers
function update_container() {
        container=$1
        echo -e "${LIGHT_GREEN}[Info] Updating container $container ${WHITE}"
        if $(array_contains ALPINE_CT $container); then
                update_container_alpine $container
        elif $(array_contains PIHOLE_CT $container); then
                update_container_pihole $container
        else
                update_container_deb $container
        fi

        echo -e "${LIGHT_BLUE}[Info] Update done. Continuing"
}

# Function to start container before updating and then stopping them again
function update_container_stopped() {
        container=$1
        echo -e "${LIGHT_BLUE}[Info] Container turned off. Starting container $container ${WHITE}"
        pct start $container
        echo -e "${LIGHT_BLUE}[Info] Sleeping for 5 seconds, for container $container ${WHITE}"
        sleep 5
        echo -e "${LIGHT_GREEN}[Info] Updating $container ${WHITE}"
        update_container $container
        echo -e "${LIGHT_BLUE}[Info] Update done. Shutting down container $container ${WHITE}"
        pct shutdown $container &
}

# Iterate through each container
for container in $containers
do
        # Get container status
        status=`pct status $container`

        # Containers to skip
        if $(array_contains SKIP_CT $container); then
                echo -e "${LIGHT_PURPLE}[Info] Skipping container $container ${WHITE}"
                continue
        fi

        # Start and update containers
        if [ "$status" == "status: stopped" ] && [ $WAKEUP_CT == "yes" ]; then
                update_container_stopped $container
        elif [ "$status" == "status: running" ]; then
                update_container $container
        else
                echo -e "${LIGHT_PURPLE}[Info] Skipping container $container ${WHITE}"
        fi
done; wait

# If we also need to update the Proxmox host
if [ $PROXMOX_UPDATE == "yes" ]; then
        apt update && apt full-upgrade -y && apt clean && apt autoremove -y
fi

